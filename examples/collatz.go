package examples

import (
	"bufio"
	"fmt"
	"os"
)

func collatz(n int) int {
	if n <= 1 {
		return n
	}

	fmt.Println(n)
	if n%2 == 0 {
		return collatz(n / 2)
	} else {
		return collatz(3*n + 1)
	}
}

func Collatz() {
	var n int
	stdin := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("Enter a number: ")
		_, err := fmt.Fscanf(stdin, "%d", &n)
		stdin.ReadString('\n')

		if err != nil {
			fmt.Println("Input is not a number!")
		} else {
			break
		}
	}

	res := collatz(n)
	fmt.Println(res)
}
