package main

import (
	"fmt"

	"codeberg.org/vollkorntomate/gotryout/examples"
)

func main() {
	var input string

	fmt.Print("Enter an example: ")
	fmt.Scan(&input)

	switch input {
	case "collatz":
		examples.Collatz()
	default:
		fmt.Println("Unknown example! Try again...")
		main()
	}
}
